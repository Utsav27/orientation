
**_BASIC GIT TERMS_**
1) **Git**- Git is version-control software that makes collaboration with teammates super simple.
2) **Repository** - A repository is the collection of files and folders (code files) that you’re using git to track.
3) **Commit** - When you commit to a repository, it’s like you’re taking picture/snapshot of the files as they exist at that moment.
4) **Push** - Syncing your commits to gitlab.
5) **Branch** - The trunk of the tree, the main software, is called the Master Branch. The branches of that tree are, well, called branches. 
            These are seperate instances of the code which are different from main codebase
6) **Merge** - Intergating two branches together.
7) **Clone** - It takes the entire online repository and makes an exact copy of it on your local machine.
8) **Fork** - Forking is a lot like cloning, only instead of making a duplicate of an existing repo on your local machine.

**_Three main stages modified, staged and commit_**
1) **modified** - you have changed the file but have not committed it to your repo yet.
2) **staged** - Staged means that you have marked a modified file in its current version to go into your next picture/snapshot.
3) **commit** - Committed means that the data is safely stored in your local repo in form of pictures/snapshots.

**_GIT SYNTAX_**
1) **Clone a repo** - _$ git clone <link-to-repository>_
2) **Create a new branch** -  
                            _$ git checkout master_  
                             _$ git checkout -b <your-branch-name>_
3) **Modify in your working tree** - _$git add ._
4) **Commit** - _$git commit -first commit_
5) **Push to online repo** - _$git push origin **branch name here**_


